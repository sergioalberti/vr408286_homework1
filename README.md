Homework 1 - Sergio Alberti VR408286
====================================

> **Nome Package**: vr408286_homework1
>
> **Launch File**: launch/vr408286_homework1.launch


La struttura adottata per la creazione del package è quella mostrata nella seguente immagine.

![ros_graph](img/ros_graph.png "ros graph")

### Nodo Publisher
Il nodo Publisher si occupa di creare il topic */students_info* su cui pubblica ad una frequenza di 1Hz. Ciò che viene pubblicato è un messaggio il cui tipo è definito nel file *msg/Student.msg* ed è così composto:

    string name
    int8 age
    string course

### Nodo Selector
Il nodo Selector usa gli stessi meccanismi del file *teleop_turtle_key.cpp* discusso a lezione per ricevere input da tastiera, legandoli alle lettere richieste dalla consegna (A, C, E, N) attraverso le seguenti macro:

    #define KEYCODE_A 0x61
    #define KEYCODE_C 0x63
    #define KEYCODE_E 0x65
    #define KEYCODE_N 0x6E

Viene quindi creato un topic chiamato */keyboard_press* su cui vengono inviati i caratteri corrispondenti ai tasti premuti dall'utente nel caso in cui corrispondano ad una delle lettere sopra citate. Il tipo di messaggio pubblicato conterrà solo un carattere, di conseguenza il tipo scelto è:

    std_msgs::Char

### Nodo End_node
L'End_node si iscrive ai due topic creati dai precedenti nodi e tramite le rispettive callback imposta il valore delle sue variabili interne. In particolare:

- le variabili **name**, **course**, **age** verranno costantemente aggiornate con i valori estratti dai messaggi in */students_info*

- la variabile **STATUS**, di tipo *char*, verrà aggiornata alla ricezione di un messaggio sul topic */keyboard_press* e indica al nodo quale parte del messaggio mostrare a schermo

La frequenza di lavoro è 1Hz, per mantenere la sincronizzazione con il nodo Publisher.

### Compilazione/Esecuzione
    cd ~/catkin_ws/src
    git clone https://sergioalberti@bitbucket.org/sergioalberti/vr408286_homework1.git
    cd ..
    catkin_make --pkg vr408286_homework1
    roslaunch vr408286_homework1 vr408286_homework1.launch
