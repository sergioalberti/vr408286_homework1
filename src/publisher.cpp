#include "ros/ros.h"
#include "vr408286_homework1/Student.h"

int main(int argc, char **argv){
	ros::init(argc, argv, "publisher");
	ros::NodeHandle n;
	
	ros::Rate rate(1);
	ros::Publisher students_topic = n.advertise<vr408286_homework1::Student>("students_info", 1000);

	vr408286_homework1::Student message;
	message.name = "Sergio Alberti";
	message.age = 25;
	message.course = "Lab. Ciberfisico";

	while(ros::ok()){
		students_topic.publish(message);
		ROS_INFO("[PUBLISHER] message published");
		rate.sleep();
	}

	return 0;
}
