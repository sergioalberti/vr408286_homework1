#include "ros/ros.h"
#include "std_msgs/Char.h"
#include "vr408286_homework1/Student.h"

using namespace std;

char STATUS = 'A';
string name="", course="";
int age;

void keyboardCallback(const std_msgs::Char::ConstPtr& msg){
	//ROS_INFO("[END_NODE] received -- %c -- from selector node", msg->data);
	STATUS = msg->data;
}

void studentInfoCallback(const vr408286_homework1::Student &rec){
	name = rec.name;
	course = rec.course;
	age = rec.age;
}

int main(int argc, char **argv){
	ros::init(argc, argv, "end_node");
	ros::NodeHandle n;
	ros::Rate rate(1);

	ros::Subscriber students_sub = n.subscribe("students_info", 1000, studentInfoCallback);
	ros::Subscriber keyboard_sub = n.subscribe("keyboard_press", 1000, keyboardCallback);

	while(ros::ok()){
		rate.sleep();
		ros::spinOnce();

		switch(STATUS){
			case 'A':
				ROS_INFO("[END_NODE] %s - %d - %s", name.c_str(), age, course.c_str());
				break;
			case 'N':
				ROS_INFO("[END_NODE] %s", name.c_str());
				break;
			case 'E':
				ROS_INFO("[END_NODE] %d", age);
				break;
			case 'C':
				ROS_INFO("[END_NODE] %s", course.c_str());
				break;
			default:
				ROS_INFO("[END_NODE] ERROR - received something wrong");
				break;
		}
	}

	return 0;
}
