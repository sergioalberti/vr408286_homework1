#include <ros/ros.h>
#include "std_msgs/Char.h"
#include <signal.h>
#include <termios.h>
#include <stdio.h>

#define KEYCODE_A 0x61
#define KEYCODE_C 0x63
#define KEYCODE_E 0x65
#define KEYCODE_N 0x6E
#define KEYCODE_Q 0x70

class Selector{
public:
	Selector();
	void keyLoop();

private:
	ros::NodeHandle n;
	ros::Publisher keyboard_topic;

};

Selector::Selector(){
	keyboard_topic = n.advertise<std_msgs::Char>("keyboard_press", 1);
}

int kfd = 0;
struct termios cooked, raw;

void quit(int sig){
	(void)sig;
	tcsetattr(kfd, TCSANOW, &cooked);
	ros::shutdown();
	exit(0);
}

int main(int argc, char** argv){
	ros::init(argc, argv, "selector");
	Selector selector;

	signal(SIGINT,quit);
	selector.keyLoop();
	return(0);
}

void Selector::keyLoop()
{
	char c;
	bool dirty=false;

	// get the console in raw mode
	tcgetattr(kfd, &cooked);
	memcpy(&raw, &cooked, sizeof(struct termios));
	raw.c_lflag &=~ (ICANON | ECHO);
	// Setting a new line, then end of file
	raw.c_cc[VEOL] = 1;
	raw.c_cc[VEOF] = 2;
	tcsetattr(kfd, TCSANOW, &raw);

	puts("Reading from keyboard");
	puts("---------------------------");
	puts("A = show the entire message");
	puts("N = show the name");
	puts("E = show the age");
	puts("C = show the course");

	for(;;){
		// get the next event from the keyboard
		if(read(kfd, &c, 1) < 0){
			perror("read():");
			exit(-1);
		}

		//ROS_INFO("value: 0x%02X\n", c);

		std_msgs::Char msg;
		switch(c){
			case KEYCODE_A:
			msg.data = 'A';
			dirty = true;
			break;

			case KEYCODE_C:
			msg.data = 'C';
			dirty = true;
			break;

			case KEYCODE_E:
			msg.data = 'E';
			dirty = true;
			break;

			case KEYCODE_N:
			msg.data = 'N';
			dirty = true;
			break;
		}

		if(dirty==true){
			ROS_INFO("[SELECTOR] message published");
			keyboard_topic.publish(msg);
			dirty=false;
		}
	}

	return;
}
